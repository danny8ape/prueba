import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BeginningComponent } from './beginning/beginning.component';
import { DashboardComponent } from './dashboard.component';
import { AddPersonComponent } from './beginning/add-person/add-person.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: '', component: BeginningComponent },
      { path: 'add-person', component: AddPersonComponent }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
