import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IPerson } from 'src/app/interfaces/person';
import { PersonsService } from 'src/app/services/persons.service';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private _personService: PersonsService,
    private router: Router
  ) {
    this.form = this.fb.group({
      fullname: ['', Validators.required],
      birth: ['', Validators.required]
    });
   }

  ngOnInit(): void {
  }

  addPerson() {
    const person: IPerson ={
      id: this.form.value.id,
      fullname: this.form.value.fullname,
      birth: this.form.value.birth
    };
    this._personService.addPerson(person);
  }

}
