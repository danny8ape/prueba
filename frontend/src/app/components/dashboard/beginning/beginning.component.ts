import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { IPerson } from 'src/app/interfaces/person';
import { PersonsService } from 'src/app/services/persons.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-beginning',
  templateUrl: './beginning.component.html',
  styleUrls: ['./beginning.component.css'],
})
export class BeginningComponent implements OnInit {
  listPersons: IPerson[] = [];
  displayedColumns: string[] = ['id', 'fullname', 'birth', 'action'];
  dataSource!: MatTableDataSource<any>;
  constructor(
    private router: Router,
    private _personService: PersonsService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.loadPersons();
    this.setListPersons([]);
  }

  async setListPersons(arrayListPersons: IPerson[]) {
    this.dataSource = new MatTableDataSource<IPerson>(arrayListPersons);
    this.cdr.detectChanges();
  }

  async loadPersons() {
      (await this._personService.getPersons()).subscribe(
        (res: IPerson[]) => {
          this.setListPersons(res);
          console.log(res);
        },
        (err) => console.log(err)
      );
      this.dataSource = new MatTableDataSource(this.listPersons);
  }

  async deletePerson(id: number) {
    await this._personService.deletePerson(id);
  }




  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
