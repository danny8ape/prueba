import { Injectable } from '@angular/core';
import { IPerson } from '../interfaces/person';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PersonsService {
  private person!: IPerson;
  url = environment.apiurl;
  personEdit: any;
  constructor(private http: HttpClient) {}

  async getPersons(): Promise<Observable<IPerson[]>> {
    return this.http.get<IPerson[]>(this.url + `/persons`);
  }

  async deletePerson(id: number): Promise<any> {
    const url = this.url + `/persons/${id}`;
    return this.http.delete(url).subscribe((res) => {
      console.log(res);
    });
  }

  async addPerson(person: IPerson) {
    return this.http.post(this.url + `/addPerson`, person).subscribe((res) => {
      console.log(res);
    });
  }

}
