export interface IPerson {
    id?: number;
    fullname?: string;
    birth?: number;
}